
CREATE SEQUENCE public.estado_cliente_id_seq;

CREATE TABLE public.estado_cliente (
                id SMALLINT NOT NULL DEFAULT nextval('public.estado_cliente_id_seq'),
                descripcion VARCHAR(15) NOT NULL,
                codigo SMALLINT NOT NULL,
                CONSTRAINT estado_cliente_pk PRIMARY KEY (id)
);
COMMENT ON TABLE public.estado_cliente IS 'Diversos estados del cliente en una cola de atencion';
COMMENT ON COLUMN public.estado_cliente.codigo IS 'Identificador a nivel gerencial.';


ALTER SEQUENCE public.estado_cliente_id_seq OWNED BY public.estado_cliente.id;

CREATE UNIQUE INDEX estado_cliente_codigo_uq
 ON public.estado_cliente
 ( codigo );

CREATE UNIQUE INDEX estado_cliente_descripcion_uq
 ON public.estado_cliente
 ( descripcion );

CREATE SEQUENCE public.prioridad_id_seq;

CREATE TABLE public.prioridad (
                id SMALLINT NOT NULL DEFAULT nextval('public.prioridad_id_seq'),
                codigo SMALLINT NOT NULL,
                nombre VARCHAR(15) NOT NULL,
                descripcion VARCHAR(30),
                CONSTRAINT prioridad_pk PRIMARY KEY (id)
);
COMMENT ON COLUMN public.prioridad.codigo IS 'codigo a nivel gerencial que define la prioridad';
COMMENT ON COLUMN public.prioridad.descripcion IS 'Descripcion donde se podra definir el tipo de clientes que se podra atender.';


ALTER SEQUENCE public.prioridad_id_seq OWNED BY public.prioridad.id;

CREATE SEQUENCE public.servicio_id_seq_1;

CREATE TABLE public.servicio (
                id SMALLINT NOT NULL DEFAULT nextval('public.servicio_id_seq_1'),
                codigo SMALLINT NOT NULL,
                descripcion VARCHAR(15) NOT NULL,
                activo BOOLEAN NOT NULL,
                CONSTRAINT servicio_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.servicio_id_seq_1 OWNED BY public.servicio.id;

CREATE UNIQUE INDEX servicio_idx
 ON public.servicio
 ( codigo );

CREATE SEQUENCE public.cola_id_seq;

CREATE TABLE public.cola (
                id SMALLINT NOT NULL DEFAULT nextval('public.cola_id_seq'),
                servicio_id SMALLINT NOT NULL,
                CONSTRAINT cola_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.cola_id_seq OWNED BY public.cola.id;

CREATE SEQUENCE public.caja_id_seq;

CREATE TABLE public.caja (
                id SMALLINT NOT NULL DEFAULT nextval('public.caja_id_seq'),
                nro_caja SMALLINT NOT NULL,
                fecha_insercion TIMESTAMP NOT NULL,
                activo BOOLEAN NOT NULL,
                fecha_cierre TIMESTAMP,
                CONSTRAINT caja_pk PRIMARY KEY (id)
);
COMMENT ON COLUMN public.caja.fecha_insercion IS 'Fecha-hora en la que la caja es creada';
COMMENT ON COLUMN public.caja.fecha_cierre IS 'Fecha en la que la caja se cierra.';


ALTER SEQUENCE public.caja_id_seq OWNED BY public.caja.id;

CREATE SEQUENCE public.permiso_id_seq;

CREATE TABLE public.permiso (
                id SMALLINT NOT NULL DEFAULT nextval('public.permiso_id_seq'),
                codigo VARCHAR(8) NOT NULL,
                descripcion VARCHAR(32) NOT NULL,
                activo BOOLEAN DEFAULT TRUE NOT NULL,
                CONSTRAINT permiso_pk PRIMARY KEY (id)
);
COMMENT ON COLUMN public.permiso.id IS 'Identificador de la entidad. Secuencia autogenerada.';
COMMENT ON COLUMN public.permiso.codigo IS 'Valor único.';
COMMENT ON COLUMN public.permiso.descripcion IS 'Nombre de rol.';
COMMENT ON COLUMN public.permiso.activo IS 'Estado de la entidad.';


ALTER SEQUENCE public.permiso_id_seq OWNED BY public.permiso.id;

CREATE SEQUENCE public.rol_id_seq;

CREATE TABLE public.rol (
                id SMALLINT NOT NULL DEFAULT nextval('public.rol_id_seq'),
                codigo VARCHAR(8) NOT NULL,
                descripcion VARCHAR(32) NOT NULL,
                activo BOOLEAN DEFAULT TRUE NOT NULL,
                CONSTRAINT rol_pk PRIMARY KEY (id)
);
COMMENT ON TABLE public.rol IS 'Tabla para registrar los roles para los actores del sistema.';
COMMENT ON COLUMN public.rol.id IS 'Identificador de la entidad. Secuencia autogenerada.';
COMMENT ON COLUMN public.rol.codigo IS 'Valor único.';
COMMENT ON COLUMN public.rol.descripcion IS 'Nombre de rol.';
COMMENT ON COLUMN public.rol.activo IS 'Estado de la entidad.';


ALTER SEQUENCE public.rol_id_seq OWNED BY public.rol.id;

CREATE SEQUENCE public.rol_permiso_id_seq;

CREATE TABLE public.rol_permiso (
                id SMALLINT NOT NULL DEFAULT nextval('public.rol_permiso_id_seq'),
                permiso_id SMALLINT NOT NULL,
                rol_id SMALLINT NOT NULL,
                CONSTRAINT rol_permiso_pk PRIMARY KEY (id)
);
COMMENT ON TABLE public.rol_permiso IS 'Tabla intermedio para referenciar roles y permisos.';
COMMENT ON COLUMN public.rol_permiso.id IS 'Identificador de la entidad. Secuencia autogenerada.';
COMMENT ON COLUMN public.rol_permiso.permiso_id IS 'Identificador de la entidad. Secuencia autogenerada.';


ALTER SEQUENCE public.rol_permiso_id_seq OWNED BY public.rol_permiso.id;

CREATE SEQUENCE public.pais_id_seq;

CREATE TABLE public.pais (
                id SMALLINT NOT NULL DEFAULT nextval('public.pais_id_seq'),
                codigo_iso2 CHAR(2) NOT NULL,
                codigo_iso3 CHAR(3) NOT NULL,
                descripcion VARCHAR(16) NOT NULL,
                activo BOOLEAN DEFAULT TRUE NOT NULL,
                fecha_insercion TIMESTAMP DEFAULT now() NOT NULL,
                usuario_insercion VARCHAR(16) NOT NULL,
                fecha_modificacion TIMESTAMP,
                usuario_modificacion VARCHAR(16),
                CONSTRAINT pais_pk PRIMARY KEY (id)
);
COMMENT ON TABLE public.pais IS 'Tabla paramétrica para registrar Paises.';
COMMENT ON COLUMN public.pais.id IS 'Identificador de la entidad, secuencia autogenerada.';
COMMENT ON COLUMN public.pais.codigo_iso2 IS 'Valor único ISO-2.';
COMMENT ON COLUMN public.pais.codigo_iso3 IS 'Valor único ISO-3.';
COMMENT ON COLUMN public.pais.descripcion IS 'Nombre del pais.';
COMMENT ON COLUMN public.pais.activo IS 'Estado de la entidad.';
COMMENT ON COLUMN public.pais.fecha_insercion IS 'Fecha de operación de la inserción.';
COMMENT ON COLUMN public.pais.usuario_insercion IS 'Nombre de usuario que realiza la operación de insertar registro.';
COMMENT ON COLUMN public.pais.fecha_modificacion IS 'Fecha de la última operación de actualización.';
COMMENT ON COLUMN public.pais.usuario_modificacion IS 'Nombre de usuario que realiza la última operación de actualizar registro.';


ALTER SEQUENCE public.pais_id_seq OWNED BY public.pais.id;

CREATE UNIQUE INDEX pais_codigo_uq
 ON public.pais USING BTREE
 ( codigo_iso2 ASC, codigo_iso3 ASC );

CREATE SEQUENCE public.nacionalidad_id_seq;

CREATE TABLE public.nacionalidad (
                id SMALLINT NOT NULL DEFAULT nextval('public.nacionalidad_id_seq'),
                pais_id SMALLINT NOT NULL,
                codigo SMALLINT NOT NULL,
                descripcion VARCHAR(24) NOT NULL,
                activo BOOLEAN DEFAULT TRUE NOT NULL,
                fecha_insercion TIMESTAMP DEFAULT now() NOT NULL,
                usuario_insercion VARCHAR(16) NOT NULL,
                fecha_modificacion TIMESTAMP,
                usuario_modificacion SMALLINT,
                CONSTRAINT nacionalidad_pk PRIMARY KEY (id)
);
COMMENT ON TABLE public.nacionalidad IS 'Tabla paramétrica: Nacionalidades.';
COMMENT ON COLUMN public.nacionalidad.id IS 'Identificador de la entidad, secuencia autogenerada.';
COMMENT ON COLUMN public.nacionalidad.pais_id IS 'Referencia (fk) a la entidad pais.';
COMMENT ON COLUMN public.nacionalidad.codigo IS 'Valor único.';
COMMENT ON COLUMN public.nacionalidad.descripcion IS 'Nombre de la naciolidad.';
COMMENT ON COLUMN public.nacionalidad.activo IS 'Estado de la entidad.';
COMMENT ON COLUMN public.nacionalidad.fecha_insercion IS 'Fecha de operación de la inserción.';
COMMENT ON COLUMN public.nacionalidad.usuario_insercion IS 'Nombre de usuario que realiza la operación de insertar registro.';
COMMENT ON COLUMN public.nacionalidad.fecha_modificacion IS 'Fecha de la última operación de actualización.';
COMMENT ON COLUMN public.nacionalidad.usuario_modificacion IS 'Nombre de usuario que realiza la última operación de actualizar registro.';


ALTER SEQUENCE public.nacionalidad_id_seq OWNED BY public.nacionalidad.id;

CREATE UNIQUE INDEX nacionalidad_codigo_uq
 ON public.nacionalidad USING BTREE
 ( codigo ASC );

CREATE SEQUENCE public.sexo_id_seq;

CREATE TABLE public.sexo (
                id SMALLINT NOT NULL DEFAULT nextval('public.sexo_id_seq'),
                codigo SMALLINT NOT NULL,
                descripcion VARCHAR(16) NOT NULL,
                activo BOOLEAN DEFAULT TRUE NOT NULL,
                fecha_insercion TIMESTAMP DEFAULT now() NOT NULL,
                usuario_insercion VARCHAR(16) NOT NULL,
                fecha_modificacion TIMESTAMP,
                usuario_modificacion VARCHAR(16),
                CONSTRAINT sexo_pk PRIMARY KEY (id)
);
COMMENT ON TABLE public.sexo IS 'Tabla paramétrica: Sexo.';
COMMENT ON COLUMN public.sexo.id IS 'Identificador de la entidad. Secuencia autogenerada.';
COMMENT ON COLUMN public.sexo.codigo IS 'Valor único.';
COMMENT ON COLUMN public.sexo.descripcion IS 'Nombre de sexo.';
COMMENT ON COLUMN public.sexo.activo IS 'Estado de la entidad.';
COMMENT ON COLUMN public.sexo.fecha_insercion IS 'Fecha de operación de la inserción.';
COMMENT ON COLUMN public.sexo.usuario_insercion IS 'Nombre de usuario que realiza la operación de insertar registro.';
COMMENT ON COLUMN public.sexo.fecha_modificacion IS 'Fecha de la última operación de actualización.';
COMMENT ON COLUMN public.sexo.usuario_modificacion IS 'Nombre de usuario que realiza la última operación de actualizar registro.';


ALTER SEQUENCE public.sexo_id_seq OWNED BY public.sexo.id;

CREATE UNIQUE INDEX sexo_codigo_uq
 ON public.sexo
 ( codigo ASC );

CREATE SEQUENCE public.tipo_documento_id_seq_1;

CREATE TABLE public.tipo_documento (
                id SMALLINT NOT NULL DEFAULT nextval('public.tipo_documento_id_seq_1'),
                codigo SMALLINT NOT NULL,
                descripcion VARCHAR(16) NOT NULL,
                activo BOOLEAN DEFAULT TRUE NOT NULL,
                fecha_insercion TIMESTAMP DEFAULT now() NOT NULL,
                usuario_insercion VARCHAR(16) NOT NULL,
                fecha_modificacion TIMESTAMP,
                usuario_modificacion VARCHAR(16),
                CONSTRAINT tipo_documento_pk PRIMARY KEY (id)
);
COMMENT ON TABLE public.tipo_documento IS 'Tabla paramétrica: Tipos de Documentos.';
COMMENT ON COLUMN public.tipo_documento.id IS 'Identificador de la entidad, secuencia autogenerada.';
COMMENT ON COLUMN public.tipo_documento.codigo IS 'Valor único.';
COMMENT ON COLUMN public.tipo_documento.descripcion IS 'Nombre del tipo del tipo de documento.';
COMMENT ON COLUMN public.tipo_documento.activo IS 'Estado de la entidad.';
COMMENT ON COLUMN public.tipo_documento.fecha_insercion IS 'Fecha de operación de la inserción.';
COMMENT ON COLUMN public.tipo_documento.usuario_insercion IS 'Nombre de usuario que realiza la operación de insertar registro.';
COMMENT ON COLUMN public.tipo_documento.fecha_modificacion IS 'Fecha de la última operación de actualización.';
COMMENT ON COLUMN public.tipo_documento.usuario_modificacion IS 'Nombre de usuario que realiza la última operación de actualizar registro.';


ALTER SEQUENCE public.tipo_documento_id_seq_1 OWNED BY public.tipo_documento.id;

CREATE UNIQUE INDEX tipo_documento_codigo_uq
 ON public.tipo_documento
 ( codigo ASC );

CREATE SEQUENCE public.tipo_persona_id_seq;

CREATE TABLE public.tipo_persona (
                id SMALLINT NOT NULL DEFAULT nextval('public.tipo_persona_id_seq'),
                codigo SMALLINT NOT NULL,
                descripcion VARCHAR(16) NOT NULL,
                activo BOOLEAN DEFAULT TRUE NOT NULL,
                fecha_insercion TIMESTAMP DEFAULT now() NOT NULL,
                usuario_insercion VARCHAR(16) NOT NULL,
                fecha_modificacion TIMESTAMP,
                usuario_modificacion VARCHAR(16),
                CONSTRAINT tipo_persona_pk PRIMARY KEY (id)
);
COMMENT ON TABLE public.tipo_persona IS 'Tabla paramétrica: Tipos de Personas.';
COMMENT ON COLUMN public.tipo_persona.id IS 'Identificador de la entidad, secuencia autogenerada.';
COMMENT ON COLUMN public.tipo_persona.codigo IS 'Valor único.';
COMMENT ON COLUMN public.tipo_persona.descripcion IS 'Nombre del tipo de persona.';
COMMENT ON COLUMN public.tipo_persona.activo IS 'Estado de la entidad.';
COMMENT ON COLUMN public.tipo_persona.fecha_insercion IS 'Fecha de la operación de insert.';
COMMENT ON COLUMN public.tipo_persona.usuario_insercion IS 'Nombre de usuario que realiza la operación de insertar registro.';
COMMENT ON COLUMN public.tipo_persona.fecha_modificacion IS 'Fecha de la última operación de actualización.';
COMMENT ON COLUMN public.tipo_persona.usuario_modificacion IS 'Nombre de usuario que realiza la última operación de actualizar registro.';


ALTER SEQUENCE public.tipo_persona_id_seq OWNED BY public.tipo_persona.id;

CREATE UNIQUE INDEX tipo_persona_codigo_uq
 ON public.tipo_persona USING BTREE
 ( codigo ASC );

CREATE SEQUENCE public.departamento_id_seq;

CREATE TABLE public.departamento (
                id SMALLINT NOT NULL DEFAULT nextval('public.departamento_id_seq'),
                pais_is SMALLINT NOT NULL,
                codigo SMALLINT NOT NULL,
                descripcion VARCHAR(16) NOT NULL,
                activo BOOLEAN DEFAULT TRUE NOT NULL,
                fecha_insercion TIMESTAMP DEFAULT now() NOT NULL,
                usuario_insercion VARCHAR(16) NOT NULL,
                fecha_modificacion TIMESTAMP,
                usuario_modificacion VARCHAR(16),
                CONSTRAINT departamento_pk PRIMARY KEY (id)
);
COMMENT ON TABLE public.departamento IS 'Tabla paramétrica: Departamentos PY.';
COMMENT ON COLUMN public.departamento.id IS 'Identificador de la entidad, secuencia autogenerada.';
COMMENT ON COLUMN public.departamento.pais_is IS 'Referencia (fk) a la entidad pais.';
COMMENT ON COLUMN public.departamento.codigo IS 'Valor único.';
COMMENT ON COLUMN public.departamento.descripcion IS 'Nombre del departamento del PY.';
COMMENT ON COLUMN public.departamento.activo IS 'Estado de la entidad.';
COMMENT ON COLUMN public.departamento.fecha_insercion IS 'Fecha de operación de la inserción.';
COMMENT ON COLUMN public.departamento.usuario_insercion IS 'Nombre de usuario que realiza la operación de insertar registro.';
COMMENT ON COLUMN public.departamento.fecha_modificacion IS 'Fecha de la última operación de actualización.';
COMMENT ON COLUMN public.departamento.usuario_modificacion IS 'Nombre de usuario que realiza la última operación de actualizar registro.';


ALTER SEQUENCE public.departamento_id_seq OWNED BY public.departamento.id;

CREATE UNIQUE INDEX departamento_codigo_iuq
 ON public.departamento USING BTREE
 ( codigo ASC );

CREATE SEQUENCE public.distrito_id_seq;

CREATE TABLE public.distrito (
                id SMALLINT NOT NULL DEFAULT nextval('public.distrito_id_seq'),
                departamento_id SMALLINT NOT NULL,
                codigo SMALLINT NOT NULL,
                descripcion VARCHAR(32) NOT NULL,
                activo BOOLEAN DEFAULT TRUE NOT NULL,
                fecha_insercion TIMESTAMP DEFAULT now() NOT NULL,
                usuario_insercion VARCHAR(16) NOT NULL,
                fecha_modificacion TIMESTAMP,
                usuario_modificacion VARCHAR(16),
                CONSTRAINT distrito_pk PRIMARY KEY (id)
);
COMMENT ON TABLE public.distrito IS 'Tabla paramétrica: Distritos PY.';
COMMENT ON COLUMN public.distrito.id IS 'Identificador de la entidad, secuencia autogenerada.';
COMMENT ON COLUMN public.distrito.departamento_id IS 'Referencia (fk) a la entidad departamento.';
COMMENT ON COLUMN public.distrito.codigo IS 'Valor único.';
COMMENT ON COLUMN public.distrito.descripcion IS 'Nombre del distrito del PY.';
COMMENT ON COLUMN public.distrito.activo IS 'Estado de la entidad.';
COMMENT ON COLUMN public.distrito.fecha_insercion IS 'Fecha de operación de la inserción.';
COMMENT ON COLUMN public.distrito.usuario_insercion IS 'Nombre de usuario que realiza la operación de insertar registro.';
COMMENT ON COLUMN public.distrito.fecha_modificacion IS 'Fecha de la última operación de actualización.';
COMMENT ON COLUMN public.distrito.usuario_modificacion IS 'Nombre de usuario que realiza la última operación de actualizar registro.';


ALTER SEQUENCE public.distrito_id_seq OWNED BY public.distrito.id;

CREATE UNIQUE INDEX distrito_codigo_uq
 ON public.distrito USING BTREE
 ( codigo ASC );

CREATE SEQUENCE public.persona_id_seq;

CREATE TABLE public.persona (
                id INTEGER NOT NULL DEFAULT nextval('public.persona_id_seq'),
                tipo_persona_id SMALLINT NOT NULL,
                tipo_documento_id SMALLINT NOT NULL,
                numero_documento VARCHAR(16) NOT NULL,
                nombre VARCHAR(64) NOT NULL,
                apellido VARCHAR(64) NOT NULL,
                fecha_nacimiento DATE NOT NULL,
                lugar_nacimiento_distrito_id SMALLINT NOT NULL,
                sexo_id SMALLINT NOT NULL,
                nacionalidad_id SMALLINT NOT NULL,
                direccion VARCHAR(70),
                telefono VARCHAR(20),
                email VARCHAR(50),
                activo BOOLEAN DEFAULT TRUE NOT NULL,
                fecha_insercion TIMESTAMP DEFAULT now() NOT NULL,
                usuario_insercion VARCHAR(16) NOT NULL,
                fecha_modificacion TIMESTAMP,
                usuario_modificacion VARCHAR(16),
                CONSTRAINT persona_pk PRIMARY KEY (id)
);
COMMENT ON TABLE public.persona IS 'Datos Personales.';
COMMENT ON COLUMN public.persona.id IS 'Identificador de la entidad, secuencia autogenerada.';
COMMENT ON COLUMN public.persona.tipo_persona_id IS 'Referencia (fk) a la entidad tipo de persona.';
COMMENT ON COLUMN public.persona.tipo_documento_id IS 'Referencia (fk) a la entidad tipo de documento.';
COMMENT ON COLUMN public.persona.numero_documento IS 'Número de documento de identidad.';
COMMENT ON COLUMN public.persona.nombre IS 'Nombre/s de la persona.';
COMMENT ON COLUMN public.persona.apellido IS 'Apellido/s de la persona.';
COMMENT ON COLUMN public.persona.lugar_nacimiento_distrito_id IS 'Referencia (fk) a la entidad distrito.';
COMMENT ON COLUMN public.persona.sexo_id IS 'Referencia (fk) a la entidad sexo.';
COMMENT ON COLUMN public.persona.nacionalidad_id IS 'Referencia (fk) a la entidad nacionalidad.';
COMMENT ON COLUMN public.persona.direccion IS 'Dirección de domicilio particular actual.';
COMMENT ON COLUMN public.persona.telefono IS 'Número telefónico particular actual.';
COMMENT ON COLUMN public.persona.email IS 'Dirección de correo electrónico particular actual.';
COMMENT ON COLUMN public.persona.activo IS 'Estado de la entidad.';
COMMENT ON COLUMN public.persona.fecha_insercion IS 'Fecha de operación de la inserción.';
COMMENT ON COLUMN public.persona.usuario_insercion IS 'Nombre de usuario que realiza la operación de insertar registro.';
COMMENT ON COLUMN public.persona.fecha_modificacion IS 'Fecha de la última operación update.';
COMMENT ON COLUMN public.persona.usuario_modificacion IS 'Nombre de usuario que realiza la última operación de actualizar registro.';


ALTER SEQUENCE public.persona_id_seq OWNED BY public.persona.id;

CREATE UNIQUE INDEX persona_nro_documento_uq
 ON public.persona USING BTREE
 ( numero_documento ASC );

CREATE SEQUENCE public.usuario_id_seq;

CREATE TABLE public.usuario (
                id INTEGER NOT NULL DEFAULT nextval('public.usuario_id_seq'),
                nombre VARCHAR(16) NOT NULL,
                clave VARCHAR(32) NOT NULL,
                activo BOOLEAN DEFAULT TRUE NOT NULL,
                persona_id INTEGER NOT NULL,
                rol_id SMALLINT NOT NULL,
                fecha_insercion TIMESTAMP DEFAULT now() NOT NULL,
                fecha_modificacion TIMESTAMP,
                CONSTRAINT usuario_pk PRIMARY KEY (id)
);
COMMENT ON TABLE public.usuario IS 'Tabla para registrar los usuarios del sistema.';
COMMENT ON COLUMN public.usuario.id IS 'Identificador de la entidad. Secuencia autogenerada.';
COMMENT ON COLUMN public.usuario.nombre IS 'Nombre único de usuario.';
COMMENT ON COLUMN public.usuario.clave IS 'Valor codificado MD5 de la contraseña de acceso al sistema.';
COMMENT ON COLUMN public.usuario.activo IS 'Estado de la entidad.';
COMMENT ON COLUMN public.usuario.fecha_insercion IS 'Fecha de operación de la inserción.';
COMMENT ON COLUMN public.usuario.fecha_modificacion IS 'Fecha de la última operación de actualización.';


ALTER SEQUENCE public.usuario_id_seq OWNED BY public.usuario.id;

CREATE UNIQUE INDEX usuario_nombre_uq
 ON public.usuario USING BTREE
 ( nombre ASC );

CREATE SEQUENCE public.apertura_caja_id_seq;

CREATE TABLE public.apertura_caja (
                id INTEGER NOT NULL DEFAULT nextval('public.apertura_caja_id_seq'),
                usuario_uso_id INTEGER NOT NULL,
                servicio_id SMALLINT NOT NULL,
                usuario_creacion_id INTEGER NOT NULL,
                fecha_usfecha_uso_inicioo TIMESTAMP NOT NULL,
                fecha_uso_fin TIMESTAMP,
                caja_id SMALLINT NOT NULL,
                arqueo INTEGER,
                CONSTRAINT apertura_caja_pk PRIMARY KEY (id)
);
COMMENT ON COLUMN public.apertura_caja.usuario_uso_id IS 'Identificador del usuario que atiende la caja';
COMMENT ON COLUMN public.apertura_caja.usuario_creacion_id IS 'Identificador del usuario que abre la caja';
COMMENT ON COLUMN public.apertura_caja.fecha_usfecha_uso_inicioo IS 'Fecha-hora en la que se habilita la caja';
COMMENT ON COLUMN public.apertura_caja.fecha_uso_fin IS 'Fecha-hora en la que se cierra la caja';
COMMENT ON COLUMN public.apertura_caja.arqueo IS 'Cantidad total de dinero recaudado';


ALTER SEQUENCE public.apertura_caja_id_seq OWNED BY public.apertura_caja.id;

CREATE SEQUENCE public.cliente_id_seq;

CREATE TABLE public.cliente (
                id INTEGER NOT NULL DEFAULT nextval('public.cliente_id_seq'),
                persona_id INTEGER NOT NULL,
                ruc VARCHAR(20),
                CONSTRAINT cliente_pk PRIMARY KEY (id)
);
COMMENT ON TABLE public.cliente IS 'Datos de Clientes.';
COMMENT ON COLUMN public.cliente.id IS 'Identificador de la entidad, secuencia autogenerada.';
COMMENT ON COLUMN public.cliente.persona_id IS 'Referencia (fk) a la entidad persona.';
COMMENT ON COLUMN public.cliente.ruc IS 'Número del registro único del contribuyente.';


ALTER SEQUENCE public.cliente_id_seq OWNED BY public.cliente.id;

CREATE SEQUENCE public.cliente_cola_id_seq;

CREATE TABLE public.cliente_cola (
                id INTEGER NOT NULL DEFAULT nextval('public.cliente_cola_id_seq'),
                prioridad SMALLINT NOT NULL,
                cola_id SMALLINT NOT NULL,
                cliente_id INTEGER NOT NULL,
                estado SMALLINT NOT NULL,
                activo BOOLEAN NOT NULL,
                CONSTRAINT cliente_cola_pk PRIMARY KEY (id)
);


ALTER SEQUENCE public.cliente_cola_id_seq OWNED BY public.cliente_cola.id;

CREATE SEQUENCE public.atencion_id_seq;

CREATE TABLE public.atencion (
                id BIGINT NOT NULL DEFAULT nextval('public.atencion_id_seq'),
                caja_atencion_id SMALLINT NOT NULL,
                cliente_atendido_id INTEGER NOT NULL,
                hora_inicio_atencion TIME NOT NULL,
                hora_fin_atencion TIMESTAMP NOT NULL,
                CONSTRAINT atencion_pk PRIMARY KEY (id)
);
COMMENT ON COLUMN public.atencion.hora_inicio_atencion IS 'Hora en la que el cliente llega a la caja.';
COMMENT ON COLUMN public.atencion.hora_fin_atencion IS 'Hora en la que finaliza la atencion.';


ALTER SEQUENCE public.atencion_id_seq OWNED BY public.atencion.id;

CREATE SEQUENCE public.derivacion_id_seq;

CREATE TABLE public.derivacion (
                id INTEGER NOT NULL DEFAULT nextval('public.derivacion_id_seq'),
                cliente_cola_id INTEGER NOT NULL,
                cola_inicio SMALLINT NOT NULL,
                cola_destino SMALLINT NOT NULL,
                motivo VARCHAR(30) NOT NULL,
                generado_por VARCHAR(15) NOT NULL,
                CONSTRAINT derivacion_pk PRIMARY KEY (id)
);
COMMENT ON COLUMN public.derivacion.motivo IS 'Motivo por el cual el cliente fue llevado de una cola a otra';
COMMENT ON COLUMN public.derivacion.generado_por IS 'Nombre del usuario quien genera una derivacion.';


ALTER SEQUENCE public.derivacion_id_seq OWNED BY public.derivacion.id;

ALTER TABLE public.cliente_cola ADD CONSTRAINT estado_cliente_cliente_cola_fk
FOREIGN KEY (estado)
REFERENCES public.estado_cliente (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.cliente_cola ADD CONSTRAINT prioridad_cliente_cola_fk
FOREIGN KEY (prioridad)
REFERENCES public.prioridad (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.apertura_caja ADD CONSTRAINT servicio_apertura_caja_fk
FOREIGN KEY (servicio_id)
REFERENCES public.servicio (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.cola ADD CONSTRAINT servicio_cola_fk
FOREIGN KEY (servicio_id)
REFERENCES public.servicio (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.cliente_cola ADD CONSTRAINT cola_cliente_cola_fk
FOREIGN KEY (cola_id)
REFERENCES public.cola (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.derivacion ADD CONSTRAINT cola_derivacion_fk
FOREIGN KEY (cola_inicio)
REFERENCES public.cola (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.derivacion ADD CONSTRAINT cola_derivacion_fk1
FOREIGN KEY (cola_destino)
REFERENCES public.cola (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.apertura_caja ADD CONSTRAINT uso_caja_caja_fk
FOREIGN KEY (caja_id)
REFERENCES public.caja (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.atencion ADD CONSTRAINT caja_atencion_fk
FOREIGN KEY (caja_atencion_id)
REFERENCES public.caja (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.rol_permiso ADD CONSTRAINT permiso_rol_permiso_fk
FOREIGN KEY (rol_id)
REFERENCES public.permiso (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.rol_permiso ADD CONSTRAINT permiso_rol_permiso_fk1
FOREIGN KEY (permiso_id)
REFERENCES public.permiso (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.rol_permiso ADD CONSTRAINT rol_rol_permiso_fk
FOREIGN KEY (rol_id)
REFERENCES public.rol (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.usuario ADD CONSTRAINT rol_usuario_fk
FOREIGN KEY (rol_id)
REFERENCES public.rol (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.departamento ADD CONSTRAINT pais_departamento_fk
FOREIGN KEY (pais_is)
REFERENCES public.pais (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.nacionalidad ADD CONSTRAINT pais_nacionalidad_fk
FOREIGN KEY (pais_id)
REFERENCES public.pais (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.persona ADD CONSTRAINT nacionalidad_persona_fk
FOREIGN KEY (nacionalidad_id)
REFERENCES public.nacionalidad (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.persona ADD CONSTRAINT sexo_persona_fk
FOREIGN KEY (sexo_id)
REFERENCES public.sexo (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.persona ADD CONSTRAINT tipo_documento_persona_fk
FOREIGN KEY (tipo_documento_id)
REFERENCES public.tipo_documento (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.persona ADD CONSTRAINT tipo_persona_persona_fk
FOREIGN KEY (tipo_persona_id)
REFERENCES public.tipo_persona (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.distrito ADD CONSTRAINT departamento_distrito_fk
FOREIGN KEY (departamento_id)
REFERENCES public.departamento (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.persona ADD CONSTRAINT distrito_persona_fk
FOREIGN KEY (lugar_nacimiento_distrito_id)
REFERENCES public.distrito (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.cliente ADD CONSTRAINT persona_cliente_fk
FOREIGN KEY (persona_id)
REFERENCES public.persona (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.usuario ADD CONSTRAINT persona_usuario_fk
FOREIGN KEY (id)
REFERENCES public.persona (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.apertura_caja ADD CONSTRAINT usuario_uso_caja_fk
FOREIGN KEY (usuario_creacion_id)
REFERENCES public.usuario (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.apertura_caja ADD CONSTRAINT usuario_apertura_caja_fk
FOREIGN KEY (usuario_uso_id)
REFERENCES public.usuario (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.cliente_cola ADD CONSTRAINT cliente_cliente_cola_fk
FOREIGN KEY (cliente_id)
REFERENCES public.cliente (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.derivacion ADD CONSTRAINT cliente_cola_derivacion_fk
FOREIGN KEY (cliente_cola_id)
REFERENCES public.cliente_cola (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.atencion ADD CONSTRAINT atencion_cliente_cola_fk
FOREIGN KEY (cliente_atendido_id)
REFERENCES public.cliente_cola (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;