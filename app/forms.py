from django import forms

class ClienteForm(forms.Form):
    ruc = forms.CharField(max_length=10, min_length=6)
