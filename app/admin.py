from django.contrib import admin

from . import models 

class CajaAdmin(admin.ModelAdmin):
    list_display = ['nro_caja','fecha_insercion','activo','fecha_cierre']
    list_editable = ['activo']

admin.site.register(models.Pais)
admin.site.register(models.Nacionalidad)
admin.site.register(models.Departamento)
admin.site.register(models.Distrito)
admin.site.register(models.TipoDocumento)
admin.site.register(models.TipoPersona)
admin.site.register(models.Sexo)
admin.site.register(models.Persona)
admin.site.register(models.Cliente)
admin.site.register(models.Usuario)
admin.site.register(models.Rol)
admin.site.register(models.RolPermiso)
admin.site.register(models.Servicio)
admin.site.register(models.Caja, CajaAdmin)
admin.site.register(models.AperturaCaja)
admin.site.register(models.Prioridad)
admin.site.register(models.EstadoCliente)
admin.site.register(models.Cola)
