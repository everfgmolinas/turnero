from django.shortcuts import render
from .forms import ClienteForm
from django.views.generic.edit import CreateView
from .models import *
# Create your views here.

def inicio(request):
    return render(request, 'inicio.html')


class IngresarCliente(CreateView):
    form_class = ClienteForm
    success_url = ''


    def post(self, request, *args, **kwargs):

        return super().post(request, *args, **kwargs)


    def form_valid(self, form):

        return super().form_valid(form)
