# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey and OneToOneField has `on_delete` set to the desired behavior
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from django.db import models


class TipoPersona(models.Model):
    id = models.SmallAutoField(primary_key=True)
    codigo = models.SmallIntegerField(unique=True)
    descripcion = models.CharField(max_length=16)
    activo = models.BooleanField()
    fecha_insercion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    usuario_modificacion = models.CharField(max_length=16, blank=True, null=True)

    def __str__(self) :
        return self.descripcion

    class Meta:
        managed = False
        db_table = 'tipo_persona'


class Caja(models.Model):
    id = models.SmallAutoField(primary_key=True)
    nro_caja = models.IntegerField(primary_key=False, unique=True)
    fecha_insercion = models.DateTimeField()
    activo = models.BooleanField()
    fecha_cierre = models.DateTimeField(blank=True, null=True)
    
    def __str__(self) :
        return 'Caja nro: ' + self.nro_caja.__str__()

    class Meta:
        managed = False
        db_table = 'caja'


class Sexo(models.Model):
    id = models.SmallAutoField(primary_key=True)
    codigo = models.SmallIntegerField(unique=True)
    descripcion = models.CharField(max_length=16)
    activo = models.BooleanField()
    fecha_insercion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    usuario_modificacion = models.CharField(max_length=16, blank=True, null=True)

    def __str__(self) :
        return self.descripcion

    class Meta:
        managed = False
        db_table = 'sexo'


class TipoDocumento(models.Model):
    id = models.SmallAutoField(primary_key=True)
    codigo = models.SmallIntegerField(unique=True)
    descripcion = models.CharField(max_length=16)
    activo = models.BooleanField()
    fecha_insercion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    usuario_modificacion = models.CharField(max_length=16, blank=True, null=True)

    def __str__(self) :
        return self.descripcion

    class Meta:
        managed = False
        db_table = 'tipo_documento'


class Rol(models.Model):
    id = models.SmallAutoField(primary_key=True)
    codigo = models.CharField(max_length=8)
    descripcion = models.CharField(max_length=32)
    activo = models.BooleanField()

    def __str__(self) :
        return self.descripcion

    class Meta:
        managed = False
        db_table = 'rol'


class Permiso(models.Model):
    id = models.SmallAutoField(primary_key=True)
    codigo = models.CharField(max_length=8)
    descripcion = models.CharField(max_length=32)
    activo = models.BooleanField()

    def __str__(self) :
        return self.descripcion

    class Meta:
        managed = False
        db_table = 'permiso'


class RolPermiso(models.Model):
    id = models.SmallAutoField(primary_key=True)
    permiso = models.ForeignKey(Permiso,on_delete=models.CASCADE)
    rol = models.ForeignKey(Rol,on_delete=models.CASCADE)

    class Meta:
        managed = False
        db_table = 'rol_permiso'


class Servicio(models.Model):
    id = models.SmallAutoField(primary_key=True)
    codigo = models.SmallIntegerField(unique=True)
    descripcion = models.CharField(max_length=15)
    activo = models.BooleanField()

    def __str__(self) :
        return self.descripcion

    class Meta:
        managed = False
        db_table = 'servicio'


class Pais(models.Model):
    id = models.SmallAutoField(primary_key=True)
    codigo_iso2 = models.CharField(max_length=2)
    codigo_iso3 = models.CharField(max_length=3)
    descripcion = models.CharField(max_length=16)
    activo = models.BooleanField()
    fecha_insercion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    usuario_modificacion = models.CharField(max_length=16, blank=True, null=True)

    def __str__(self) :
        return self.descripcion

    class Meta:
        managed = False
        db_table = 'pais'
        unique_together = (('codigo_iso2', 'codigo_iso3'),)


class Departamento(models.Model):
    id = models.SmallAutoField(primary_key=True)
    pais_is = models.ForeignKey(Pais, db_column='pais_is', on_delete=models.CASCADE)
    codigo = models.SmallIntegerField(unique=True)
    descripcion = models.CharField(max_length=16)
    activo = models.BooleanField()
    fecha_insercion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    usuario_modificacion = models.CharField(max_length=16, blank=True, null=True)

    def __str__(self) :
        return self.descripcion

    class Meta:
        managed = False
        db_table = 'departamento'


class Distrito(models.Model):
    id = models.SmallAutoField(primary_key=True)
    departamento = models.ForeignKey(Departamento,on_delete=models.CASCADE)
    codigo = models.SmallIntegerField(unique=True)
    descripcion = models.CharField(max_length=32)
    activo = models.BooleanField()
    fecha_insercion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    usuario_modificacion = models.CharField(max_length=16, blank=True, null=True)

    def __str__(self) :
        return self.descripcion

    class Meta:
        managed = False
        db_table = 'distrito'


class Nacionalidad(models.Model):
    id = models.SmallAutoField(primary_key=True)
    pais = models.ForeignKey(Pais, on_delete=models.CASCADE)
    codigo = models.SmallIntegerField(unique=True)
    descripcion = models.CharField(max_length=24)
    activo = models.BooleanField()
    fecha_insercion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    usuario_modificacion = models.SmallIntegerField(blank=True, null=True)

    def __str__(self) :
        return self.descripcion

    class Meta:
        managed = False
        db_table = 'nacionalidad'


class Persona(models.Model):
    tipo_persona = models.ForeignKey(TipoPersona, on_delete=models.CASCADE)
    tipo_documento = models.ForeignKey(TipoDocumento, on_delete=models.CASCADE)
    numero_documento = models.CharField(unique=True, max_length=16)
    nombre = models.CharField(max_length=64)
    apellido = models.CharField(max_length=64)
    fecha_nacimiento = models.DateField()
    lugar_nacimiento_distrito = models.ForeignKey(Distrito,on_delete=models.CASCADE)
    sexo = models.ForeignKey(Sexo, on_delete=models.CASCADE)
    nacionalidad = models.ForeignKey(Nacionalidad,on_delete=models.CASCADE)
    direccion = models.CharField(max_length=70, blank=True, null=True)
    telefono = models.CharField(max_length=20, blank=True, null=True)
    email = models.CharField(max_length=50, blank=True, null=True)
    activo = models.BooleanField()
    fecha_insercion = models.DateTimeField()
    usuario_insercion = models.CharField(max_length=16)
    fecha_modificacion = models.DateTimeField(blank=True, null=True)
    usuario_modificacion = models.CharField(max_length=16, blank=True, null=True)

    def __str__(self) :
        return self.nombre + self.apellido

    class Meta:
        managed = False
        db_table = 'persona'


class Usuario(models.Model):
    id = models.OneToOneField(Persona,db_column='id', primary_key=True, on_delete=models.CASCADE)
    nombre = models.CharField(unique=True, max_length=16)
    clave = models.CharField(max_length=32)
    activo = models.BooleanField()
    persona_id = models.IntegerField()
    rol = models.ForeignKey(Rol,on_delete=models.CASCADE)
    fecha_insercion = models.DateTimeField()
    fecha_modificacion = models.DateTimeField(blank=True, null=True)

    def __str__(self) :
        return self.nombre

    class Meta:
        managed = False
        db_table = 'usuario'


class AperturaCaja(models.Model):
    usuario_uso = models.ForeignKey(Usuario, on_delete=models.CASCADE, related_name='user')
    servicio = models.ForeignKey(Servicio, on_delete=models.CASCADE)
    usuario_creacion = models.ForeignKey(Usuario, on_delete=models.CASCADE, related_name='user_creation')
    fecha_usfecha_uso_inicioo = models.DateTimeField()
    fecha_uso_fin = models.DateTimeField(blank=True, null=True)
    caja = models.ForeignKey(Caja, on_delete=models.CASCADE)
    arqueo = models.IntegerField(blank=True, null=True)

    def __str__(self) :
        return self.caja.nro_caja.__str__() + self.servicio.descripcion + self.usuario_uso.id.nombre

    class Meta:
        managed = False
        db_table = 'apertura_caja'


class Cliente(models.Model):
    persona = models.ForeignKey(Persona,on_delete=models.CASCADE, null=True)
    ruc = models.CharField(max_length=20, blank=True, null=True, unique=True)

    def __str__(self) :
        return self.ruc

    class Meta:
        managed = False
        db_table = 'cliente'


class Cola(models.Model):
    id = models.SmallAutoField(primary_key=True)
    servicio = models.ForeignKey(Servicio, on_delete=models.CASCADE)

    def __str__(self) :
        return self.servicio.descripcion

    class Meta:
        managed = False
        db_table = 'cola'


class EstadoCliente(models.Model):
    id = models.SmallAutoField(primary_key=True)
    descripcion = models.CharField(unique=True, max_length=15)
    codigo = models.SmallIntegerField(unique=True)

    def __str__(self) :
        return self.descripcion

    class Meta:
        managed = False
        db_table = 'estado_cliente'


class Prioridad(models.Model):
    id = models.SmallAutoField(primary_key=True)
    codigo = models.SmallIntegerField()
    nombre = models.CharField(max_length=15)
    descripcion = models.CharField(max_length=30, blank=True, null=True)

    def __str__(self) :
        return self.nombre

    class Meta:
        managed = False
        db_table = 'prioridad'


class ClienteCola(models.Model):
    prioridad = models.ForeignKey(Prioridad, db_column='prioridad', on_delete=models.CASCADE)
    cola = models.ForeignKey(Cola, on_delete=models.CASCADE)
    cliente = models.ForeignKey(Cliente,on_delete=models.CASCADE)
    estado = models.ForeignKey(EstadoCliente, db_column='estado', on_delete=models.CASCADE)
    activo = models.BooleanField()

    class Meta:
        managed = False
        db_table = 'cliente_cola'


class Derivacion(models.Model):
    cliente_cola = models.ForeignKey(ClienteCola, on_delete=models.CASCADE)
    cola_inicio = models.ForeignKey(Cola, db_column='cola_inicio', on_delete=models.CASCADE, related_name='cola_origen')
    cola_destino = models.ForeignKey(Cola, db_column='cola_destino', on_delete=models.CASCADE, related_name='cola_destino')
    motivo = models.CharField(max_length=30)
    generado_por = models.CharField(max_length=15)

    class Meta:
        managed = False
        db_table = 'derivacion'


class Atencion(models.Model):
    id = models.BigAutoField(primary_key=True)
    cliente_atendido = models.ForeignKey(ClienteCola, on_delete=models.CASCADE)
    caja_atencion = models.ForeignKey(AperturaCaja, on_delete=models.CASCADE)
    hora_inicio_atencion = models.TimeField()
    hora_fin_atencion = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'atencion'

