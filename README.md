# Requerimientos #

- Python 3.11.2
- Postgres 14

## Instalacion ##

Al clonar el proyecto crear un entorno virtual de Python  con el comando ` python -m venv env  `.  
Dentro del mismo directorio, ingresar al env con el comando `source ./env/bin/activate`.  
Instalar django con el comando `pip install django`, instalar el controlador para conectarse con postgres `pip install psycopg2-binary`.

## Configuacion del postgres ##

Conectarse al postgres y crear una base de datos denomidada "turnero", dentro del archivo `turnero/settings` modificar la contraseña de acceso por la de su postgres.

## Carga de tablas ##

Cargue la base de datos con el sql `tables.sql`

## Uso del proyecto ##

Ejecute con el comando `python3 manage.py runserver` y acceda desde la url `http://127.0.0.1:8000/`
